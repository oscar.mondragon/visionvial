#!/usr/bin/env python2

from flask import Flask, render_template, flash, redirect, url_for, session, logging, request, Response
from flask_mysqldb import MySQL
from wtforms import Form, StringField, TextAreaField, PasswordField, validators
from passlib.hash import sha256_crypt
from functools import wraps

from jinja2 import Environment
from jinja2.loaders import FileSystemLoader
import subprocess as sub
import os
import sys
import time

import gc

from  flask_socketio import SocketIO
from  threading import Lock
async_mode = 'threading'
thread = None
thread_lock = Lock()

gc.collect()
sys.path.insert(0, '/var/www/mintic/accpatterns/pythonPrograms')
import mainKalman
##from camera import VideoCamera

UPLOAD_FOLDER = '/var/www/mintic/accpatterns/videoInputs'
OUTPUT_FOLDER = '/var/www/mintic/accpatterns/static'
log_file = '/tmp/logs.txt'

app = Flask(__name__, static_url_path='')
socketio= SocketIO(app, async_mode=async_mode)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['OUTPUT_FOLDER'] = OUTPUT_FOLDER

app.secret_key='secret123'
app.config['SESSION_TYPE'] = 'filesystem'
#sess.init_app(app)


# Config MySQL

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'root'
app.config['MYSQL_DB'] = 'accident_patterns'
app.config['MYSQL_CURSOSCLASS'] = 'DictCursor'

# init MySQL
mysql = MySQL(app)


#Index
@app.route('/')
def index():
	return render_template('home.html')

#Video
@app.route('/video')
def video():
	app.send_static_file('camara5.mp4')




#About
@app.route('/about')
def about():
		return render_template('about.html')


# Register Form Class
class RegisterForm(Form):
	name = StringField('Nombre', [validators.length(min=1, max=50)])
	username = StringField('Nombre de Usuario', [validators.length(min=4, max=25)])
	email = StringField('Email', [validators.length(min=6, max=50)])
	password = PasswordField('Password', [
		validators.DataRequired(),
		validators.EqualTo('confirm', message='Password do not match')
		])
	confirm = PasswordField('Confirmar Password')

# User Register
@app.route('/register', methods=['GET', 'POST'] )
def register():
	form = RegisterForm(request.form)
	if request.method == 'POST' and form.validate():
		name = form.name.data
		email = form.email.data
		username = form.username.data
		password = sha256_crypt.encrypt(str(form.password.data))

		# Create Cursor
		cur = mysql.connection.cursor()
		cur.execute("INSERT INTO users(name,email,username,password) VALUES(%s, %s, %s, %s)", (name,email,username,password))

		# Commit to DB
		mysql.connection.commit()

		# Close connection
		cur.close()

		flash('Your are now registered and can log in', 'success')
		return redirect(url_for('login'))

	return render_template('register.html', form=form)

# User logging
@app.route('/login', methods=['GET', 'POST'])
def login():
		if request.method == 'POST':
			# Get Form Fields
			username = request.form['username']
			password_candidate = request.form['password']

			# Create a Cursor
			cur = mysql.connection.cursor()

			# Get user by Username

			result = cur.execute("SELECT * FROM users WHERE username =%s",[username])

			if result > 0:
				# Get stored hash
				data = cur.fetchone()
				password = data[4]
				#password = data['password']

				# Compare Passwords
				if sha256_crypt.verify(password_candidate, password):
					# Passed
					session['logged_in'] = True
					session['username'] = username

					flash('You are now logged in', 'success')
					return redirect(url_for('dashboard'))

					app.logger.info('PASSWORD MATCHED')

				else:
					error = 'Invalid login'
					return render_template('login.html', error=error)
					app.logger.info('PASSWORD NO MATCHED')

				# Close the connection
				cur.close()

			else:
				error = 'Username not found'
				return render_template('login.html', error=error)
				app.logger.info('NO USER')

		return render_template('login.html')

# Check if user logged in
def is_logged_in(f):
	@wraps(f)
	def wrap(*args, **kwargs):
			if 'logged_in' in session:
					return f(*args, **kwargs)
			else:
				flash('Unauthorized, Please Login', 'danger')
			return redirect(url_for('login'))
	return wrap


#Logout
@app.route('/logout')
@is_logged_in
def logout():
	session.clear()
	flash('You are now logged out', 'success')
	return redirect(url_for('login'))


# Dashboard
@app.route('/dashboard')
@is_logged_in
def dashboard():
	# Create Cursor
	cur = mysql.connection.cursor()
	# Get Events
	result = cur.execute("SELECT * FROM accident_events ORDER BY id DESC LIMIT 5")
	events = cur.fetchall()

	if result > 0:
		return render_template('dashboard.html', events = events)
	else:
		msg = 'No Events Found'
		return render_template('dashboard.html', msg = msg)
	# Close the connection
	cur.close()

@app.route('/statistics_req')
@is_logged_in
def statistics_req():
	# Create Cursor
	cur = mysql.connection.cursor()
	# Get Locations
	result = cur.execute("SELECT * FROM locations")
	locations = cur.fetchall()

	if result > 0:
		return render_template('statistics_req.html', locations = locations)
	else:
		msg = 'No locations Found'
		return render_template('dashboard.html', msg = msg)
	# Close the connection
	cur.close()


# Statistics Class
class Statistics:
	speed = 0
	direction = 0
	speed_percent = 0
	direction_percent = 0
	moto_moto = 0
	carro_moto = 0
	carro_carro = 0

def getPatternStatPercent( name, initial_date, end_date, location ):
	cur = mysql.connection.cursor()
	cur.execute("SELECT * FROM patterns WHERE name = %s", [name])
	id_pattern = cur.fetchone()
	result = cur.execute("SELECT * FROM accident_events WHERE CAST(time as DATE) BETWEEN %s AND %s AND id_pattern=%s AND accident='1' AND id_location=%s",(initial_date, end_date, id_pattern[0], location)) # not inclusive
	patterns = cur.fetchall()
	cur.close()
	return cur.rowcount


def getPatternStat( name, initial_date, end_date, location ):
	cur = mysql.connection.cursor()
	cur.execute("SELECT * FROM patterns WHERE name = %s", [name])
	id_pattern = cur.fetchone()
	result = cur.execute("SELECT * FROM accident_events WHERE CAST(time as DATE) BETWEEN %s AND %s AND id_pattern=%s AND id_location=%s",(initial_date, end_date, id_pattern[0], location)) # not inclusive
	patterns = cur.fetchall()
	cur.close()
	return cur.rowcount

def getVehicleStat( name, initial_date, end_date, location ):
	cur = mysql.connection.cursor()
	cur.execute("SELECT * FROM vehicle_combinations WHERE name = %s", [name])
	id_combination = cur.fetchone()
	result = cur.execute("SELECT * FROM accident_events WHERE CAST(time as DATE) BETWEEN %s AND %s AND id_vehicles=%s AND accident='1' AND id_location=%s",(initial_date, end_date, id_combination[0], location)) # not inclusive
	patterns = cur.fetchall()
	cur.close()
	return cur.rowcount


@app.route('/statistics_res', methods=['GET', 'POST'])
@is_logged_in
def statistics_res():
	# Get Form Fields
	location = request.form['location']
	initial_date = request.form['initial_date']
	end_date = request.form['end_date']

	result = 0

	speed = getPatternStat( 'exceso_velocidad', initial_date, end_date, location )
	direction = getPatternStat( 'direccion_abrupta', initial_date, end_date, location )

	speed_accident = getPatternStatPercent( 'exceso_velocidad', initial_date, end_date, location )
	direction_accident = getPatternStatPercent( 'direccion_abrupta', initial_date, end_date, location )

	moto_moto = getVehicleStat( 'moto_moto', initial_date, end_date, location )
	carro_moto = getVehicleStat( 'carro_moto', initial_date, end_date, location )
	carro_carro = getVehicleStat( 'carro_carro', initial_date, end_date, location )

	stats = Statistics()
	stats.speed = speed
	stats.direction = direction
	
	if speed != 0:
		speed_percent = 100 * speed_accident / speed
	else:	
		speed_percent = 0
	if direction != 0:
		direction_percent = 100 * direction_accident / direction
	else:
		direction_percent = 0
	
	stats.moto_moto = moto_moto
	stats.carro_moto = carro_moto
	stats.carro_carro = carro_carro

	stats.speed_percent = speed_percent
	stats.direction_percent = direction_percent

	if speed or direction or speed_percent or direction_percent or moto_moto or carro_moto or carro_carro:
		result = 1

	if result > 0:
		return render_template('statistics_res.html', stats = stats)
	else:
		msg = 'No events Found'
		return render_template('dashboard.html', msg = msg)
	# Close the connection

# videoRegister
def make_tree(path):
    tree = dict(name=os.path.basename(path), children=[])
    try: lst = os.listdir(path)
    except OSError:
        pass #ignore errors
    else:
        for name in lst:
            fn = os.path.join(path, name)
            if os.path.isdir(fn):
                tree['children'].append(make_tree(fn))
            else:
                tree['children'].append(dict(name=name))
    return tree





@app.route('/videoregister')
def upload_video():
	path = UPLOAD_FOLDER #os.path.expanduser(u'~')
	patho = OUTPUT_FOLDER

	return render_template('videoregister.html', tree=make_tree(path),treeo=make_tree(patho))

@app.route('/videoregister', methods=['GET', 'POST'])
def uploader_video():
	if request.form['action'] =='Procesar':
		vidsel = request.form.get('videosel')
		command='python /var/www/mintic/accpatterns/pythonPrograms/mainKalman.py' + ' ' + vidsel
		sub.call([command], shell=True)
		vidselo = os.path.splitext(vidsel)[0]
		ov = 'o'+vidselo+'.mp4'
		return render_template('videoutput.html', ov=ov)
	elif request.form['action'] =='Procesado':
		vidselo= request.form.get('videoselo')
		return render_template('videoutput.html', ov=vidselo)

if __name__ == '__main__':
	app.secret_key='secret123'
	app.config['SESSION_TYPE'] = 'filesystem'
	sess.init_app(app)
	app.debug=True
	app.run()
