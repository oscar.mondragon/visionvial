#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""@author: walter
"""

import numpy as np
import matplotlib.pyplot as plt
import glob
from moviepy.editor import VideoFileClip
from collections import deque
from sklearn.utils.linear_assignment_ import linear_assignment
import sys, os
import cv2
import math
import simplejson
from random import randint

import helpers
import SelectStreets
import Perspective
import detector
import tracker
import copy

import time
import pylab
import structures
import MySQLdb


# ----hadoop-------
# import hdfs
# from hdfs.client import InsecureClient
# import pyspark
# from pyspark import SparkContext
# conf = pyspark.SparkConf()
#
# master_url = 'http://192.168.234.2:9870'

#----------------------------------------

Polynomial = np.polynomial.Polynomial


frame_count = 0 # frame counter

max_age = 12 #12  # no.of consecutive unmatched detection before
     # a track is deleted

min_hits =1  # no. of consecutive matches needed to establish a track

scale=np.float(146/99)

frameRate =10#30

pathVideo=""

matrixTransf=[]

streetList=[]

global local

streetInOneDir=False
direction=""

tracker_list =[] # list for trackers
trackerAccidentedList=[]
tracker_list_10f=[]
# list for track ID
track_id_list= deque(['AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK','AL','AM',
		'AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
		'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK','BL','BM',
		'BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
		'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK','CL','CM',
		'CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ',
		'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK','DL','DM',
		'DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ',
		'EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK','EL','EM',
		'EN','EO','EP','EQ','ER','ES','ET','EU','EV','EW','EX','EY','EZ',
		'FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG', 'FH', 'FI', 'FJ', 'FK','FL','FM',
		'FN','FO','FP','FQ','FR','FS','FT','FU','FV','FW','FX','FY','FZ'])

debug = False #True
withImages=False
withDarknet = True
velConstant = True # cambiar tambien en el tracker.py

db = MySQLdb.connect(host="localhost",user="root",passwd="root",db="accident_patterns")
cur = db.cursor()

def assign_streets_to_trackers(trk,maskcar,box_car):
    iooalist=[]
    for idx,maskstreet in enumerate(streetList):
        iooa=rate_iooa(maskstreet.mask,maskcar,box_car)
        iooalist.append(iooa)

    if len(iooalist)!=0:
        maxiooa = max(iooalist)
        maxiooas=sorted( [(x,i) for (i,x) in enumerate(iooalist)], reverse=True )[:3]
        if len(iooalist)==1:
            if maxiooa>0.0:
                street=iooalist.index(maxiooa)
                trk.street=streetList[street]
                if len(trk.histoStreet)>0:
                    if trk.histoStreet[-1]=="NaN":
                        trk.histoStreet[-1]=streetList[street].id
                    else:
                        trk.histoStreet.append(streetList[street].id)
                else:
                    trk.histoStreet.append(streetList[street].id)
            #     #print "calle: ",street," carro: ",trk.id
            # else:
        elif len(iooalist)>1:
            maxiooa_1=maxiooas[0]
            maxiooa_2=maxiooas[1]
            file=open("logs.txt","a")
            file.write("-------------------------------------\n")
            #file.write("Todos: \n".join(maxiooas))
            simplejson.dump(maxiooas, file)
            file.write("max 1: %f\n" %maxiooa_1[0])
            file.write("idx 1: %f\n" %maxiooa_1[1])
            file.write("max 2: %f\n" %maxiooa_2[0])
            file.write("idx 2: %f\n" %maxiooa_2[1])
            file.write("-------------------------------------\n")
            file.close()

            if maxiooa_1[0]>0.7 and maxiooa_2[0]>=0.1:
                if streetList[maxiooa_1[1]].direction=="V" and streetList[maxiooa_2[1]].direction=="V" and trk.direction=="V":
                    street=maxiooa_1[1]
                    trk.street=streetList[street]
                    if len(trk.histoStreet)>0:
                        if trk.histoStreet[-1]=="NaN":
                            trk.histoStreet[-1]=streetList[street].id
                        else:
                            trk.histoStreet.append(streetList[street].id)
                    else:
                        trk.histoStreet.append(streetList[street].id)
                    #print "calle: ",street," carro: ",trk.id
                elif streetList[maxiooa_1[1]].direction=="H" and streetList[maxiooa_2[1]].direction=="V" and trk.direction=="V":
                    street=maxiooa_2[1]
                    trk.street=streetList[street]
                    if len(trk.histoStreet)>0:
                        if trk.histoStreet[-1]=="NaN":
                            trk.histoStreet[-1]=streetList[street].id
                        else:
                            trk.histoStreet.append(streetList[street].id)
                    else:
                        trk.histoStreet.append(streetList[street].id)
                elif streetList[maxiooa_1[1]].direction=="H" and streetList[maxiooa_2[1]].direction=="V" and trk.direction=="H":
                    street=maxiooa_1[1]
                    trk.street=streetList[street]
                    if len(trk.histoStreet)>0:
                        if trk.histoStreet[-1]=="NaN":
                            trk.histoStreet[-1]=streetList[street].id
                        else:
                            trk.histoStreet.append(streetList[street].id)
                    else:
                        trk.histoStreet.append(streetList[street].id)
                elif streetList[maxiooa_1[1]].direction=="V" and streetList[maxiooa_2[1]].direction=="H" and trk.direction=="V":
                    street=maxiooa_1[1]
                    trk.street=streetList[street]
                    if len(trk.histoStreet)>0:
                        if trk.histoStreet[-1]=="NaN":
                            trk.histoStreet[-1]=streetList[street].id
                        else:
                            trk.histoStreet.append(streetList[street].id)
                    else:
                        trk.histoStreet.append(streetList[street].id)
                elif streetList[maxiooa_1[1]].direction=="V" and streetList[maxiooa_2[1]].direction=="H" and trk.direction=="H":
                    street=maxiooa_2[1]
                    trk.street=streetList[street]
                    if len(trk.histoStreet)>0:
                        if trk.histoStreet[-1]=="NaN":
                            trk.histoStreet[-1]=streetList[street].id
                        else:
                            trk.histoStreet.append(streetList[street].id)
                    else:
                        trk.histoStreet.append(streetList[street].id)
                elif trk.direction=="":
                    street=maxiooa_1[1]
                    trk.street=streetList[street]
                    if len(trk.histoStreet)>0:
                        if trk.histoStreet[-1]=="NaN":
                            trk.histoStreet[-1]=streetList[street].id
                        else:
                            trk.histoStreet.append(streetList[street].id)
                    else:
                        trk.histoStreet.append(streetList[street].id)
                else:
                    streetNaN=copy.copy(streetList[0])
                    streetNaN.id=30
                    trk.street=streetNaN
                    trk.histoStreet.append("NaN")

            elif maxiooa_1[0]>maxiooa_2[0] and maxiooa_2[0]<=0.1 and maxiooa_2[0]>=0.0 and maxiooa_1[0]>0.3:
                street=maxiooa_1[1]
                trk.street=streetList[street]
                if len(trk.histoStreet)>0:
                    if trk.histoStreet[-1]=="NaN":
                        trk.histoStreet[-1]=streetList[street].id
                    else:
                        trk.histoStreet.append(streetList[street].id)
                else:
                    trk.histoStreet.append(streetList[street].id)
                #print "calle: ",street," carro: ",trk.id
            else:
                streetNaN=copy.copy(streetList[0])
                streetNaN.id=30
                trk.street=streetNaN
                trk.histoStreet.append("NaN")
                #print "no encotrada"
        else:

            streetNaN=copy.copy(streetList[0])
            streetNaN.id=30
            trk.street=streetNaN
            trk.histoStreet.append("NaN")
            #print "no encotrada"



def rate_iooa(maskstreet,maskcar,box_car):
    areaObj=cv2.contourArea(box_car)
    mas=maskstreet*maskcar
    edged = cv2.Canny(mas, 5,80)
    _, contours, _=cv2.findContours(edged, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    #cv2.drawContours(self.im, contours, -1, (0,255,0),1)
    if len(contours)>0:
        areaInter = cv2.contourArea(contours[0])
    else:
        areaInter=0
    iooa=areaInter/areaObj
    return iooa

def box_iou2(a, b):
    '''
    Helper funciton to calculate the ratio between intersection and the union of
    two boxes a and b
    a[0], a[1], a[2], a[3] <-> left, up, right, bottom
    '''

    w_intsec = np.maximum (0, (np.minimum(a[2], b[2]) - np.maximum(a[0], b[0])))
    h_intsec = np.maximum (0, (np.minimum(a[3], b[3]) - np.maximum(a[1], b[1])))
    s_intsec = w_intsec * h_intsec
    s_a = (a[2] - a[0])*(a[3] - a[1])
    s_b = (b[2] - b[0])*(b[3] - b[1])

    return float(s_intsec)/(s_a + s_b -s_intsec)

def car_direction(trk,img):

    centroids=trk.centroidTrans2D
    if len(centroids)<4:
        centroid1=centroids[-2]
        centroid2=centroids[-1]
    else:
        centroid1=centroids[-4]
        centroid2=centroids[-1]

    #centroid1=centroids[-2]
    #centroid2=centroids[-1]

    cx1=centroid1[0]
    cy1=centroid1[1]
    cx2=centroid2[0]
    cy2=centroid2[1]


    # cv2.line(img,(int(cx1),int(cy1)),(int(cx2),int(cy2)), 255, 2)

    radians=math.atan2((cx1-cx2),(cy2-cy1)) # (y,x)=(y/x)
    angle=math.degrees(radians)

    if math.sin(radians)!=0:
        m=-math.cos(radians)/math.sin(radians)
    else:
        m=200


    if angle<0:
        angle=360+angle

    trk.angleDirection.append(angle)

    if angle<=35 and angle>=325:
        trk.direction="V"
	trk.carWay="down"
    elif angle<=247 and angle>=130:
        trk.direction="V"
	trk.carWay="up"
    elif angle<325 and angle>247:
        trk.direction="H"
	trk.carWay="right"
    elif angle<130 and angle>35:
        trk.direction="H"
	trk.carWay="left"


    # print "-------------------"
    # print "angle ",angle,"pendiente ",m,"traker ",trk.id
    # print trk.direction
    # print "-------------------"

    return img

def assign_detections_to_trackers(trackers, detections, iou_thrd = 0.3):
    '''
    From current list of trackers and new detections, output matched detections,
    unmatchted trackers, unmatched detections.
    '''

    IOU_mat= np.zeros((len(trackers),len(detections)),dtype=np.float32)
    for t,trk in enumerate(trackers):
        #trk = convert_to_cv2bbox(trk)
        for d,det in enumerate(detections):
         #   det = convert_to_cv2bbox(det)
            IOU_mat[t,d] = box_iou2(trk,det)

    # Produces matches
    # Solve the maximizing the sum of IOU assignment problem using the
    # Hungarian algorithm (also known as Munkres algorithm)

    matched_idx = linear_assignment(-IOU_mat)

    unmatched_trackers, unmatched_detections = [], []
    for t,trk in enumerate(trackers):
        if(t not in matched_idx[:,0]):
            unmatched_trackers.append(t)

    for d, det in enumerate(detections):
        if(d not in matched_idx[:,1]):
            unmatched_detections.append(d)

    matches = []

    # For creating trackers we consider any detection with an
    # overlap less than iou_thrd to signifiy the existence of
    # an untracked object

    for m in matched_idx:
        if(IOU_mat[m[0],m[1]]<iou_thrd):
            unmatched_trackers.append(m[0])
            unmatched_detections.append(m[1])
        else:
            matches.append(m.reshape(1,2))

    if(len(matches)==0):
        matches = np.empty((0,2),dtype=int)
    else:
        matches = np.concatenate(matches,axis=0)

    return matches, np.array(unmatched_detections), np.array(unmatched_trackers)

def calculate_centroid(xx,tmp_trk,warped):
    cx=xx[1]+((xx[3]-xx[1])/2)
    cy=xx[0]+((xx[2]-xx[0])/2)
    centroid=np.array([cx,cy])

    if len(tmp_trk.centroids)>1:
	slwindow=(tmp_trk.centroids[-1]+centroid)/2
	centroid=slwindow

    centroidIni=np.expand_dims(np.array([centroid[0],centroid[1],1]), axis=0).T
    centroidTrans=matrixTransf.dot(centroidIni)
    #centroidTrans2D=centroidTrans
    centroidTrans2D = centroidTrans / np.tile(centroidTrans[-1, :], (3, 1)) # Divide todo por el valor último del vector.
    centroidTrans2D=centroidTrans2D[:2,0]
    file=open("logs.txt","a")
    file.write("centroidIni\n")
    file.write("matrixTransf\n")
    np.savetxt(file, np.vstack(matrixTransf), fmt='%1.10f')
    file.write("-------------------------------------\n")
    file.write("centroide transformado \n")
    np.savetxt(file, np.vstack(centroidTrans2D), fmt='%1.10f')


    if tmp_trk.id=="AV":
	vector=tmp_trk.centroidTrans2D
	warped= helpers.draw_poliLine_traker(warped,tmp_trk.centroidTrans2D,tmp_trk.box_color)
	#cv2.circle(warped, (centroidTrans2D[0],centroidTrans2D[1]), 2, [255], -1)



    centroid_old=tmp_trk.centroids[len(tmp_trk.centroids)-1]
    centroidTrans2Dold=tmp_trk.centroidTrans2D[len(tmp_trk.centroidTrans2D)-1]

    distancePix=np.float(np.sqrt(sum((centroid-centroid_old)**2)))
    distancePixTra=np.float(np.sqrt(sum((centroidTrans2D-centroidTrans2Dold)**2)))

    file.write("distance: %f\n" %distancePix)
    np.savetxt(file, np.vstack(centroid), fmt='%1.10f')
    np.savetxt(file, np.vstack(centroid_old), fmt='%1.10f')
    file.write("distanceTra: %f\n" %distancePixTra)
    file.write("-------------------------------------\n")
    file.close()

    success=0
    #0.9
    #if distancePix>=0:
    if tmp_trk.matched[-1]==1:
        tmp_trk.centroids.append(centroid)
        tmp_trk.centroidTrans2D.append(centroidTrans2D)
	tmp_trk.posx.append(centroidTrans2D[0])
	tmp_trk.posy.append(centroidTrans2D[1])
        tmp_trk.frameNum.append(frame_count)
        success=1

    return warped,tmp_trk,success,distancePixTra

def calculate_velocity(tmp_trk,distancePix):

    if tmp_trk.direction=="V":
    	distancePosPix=abs(tmp_trk.posy[-1]-tmp_trk.posy[-2])
    elif tmp_trk.direction=="H":
    	distancePosPix=abs(tmp_trk.posx[-1]-tmp_trk.posx[-2])
    distanceUni=np.float(distancePosPix*scale)
    #distanceUni=np.float(distancePix*scale)

    framebefore=tmp_trk.frameNum[len(tmp_trk.frameNum)-2]
    timebefore=(np.float(tmp_trk.frameNum[len(tmp_trk.frameNum)-2])/np.float(frameRate))

    timeTravel=(np.float(frame_count)/np.float(frameRate))-timebefore

    #velo=np.float(distanceUni/timeTravel)
    instanVelocity=np.float(distanceUni/timeTravel)

    if len(tmp_trk.velocity)>1:
        slwindow=(tmp_trk.velocity[-1]+instanVelocity)/2
        instanVelocity=slwindow

    tmp_trk.velocity.append(instanVelocity)
    file=open("logs.txt","a")
    file.write("-------------------------------------\n")
    file.write("distacia: %f\n" %distanceUni)
    file.write("tiempo total: %f\n" %timeTravel)
    file.write("tiempo despues: %f\n" %(np.float(frame_count)/np.float(frameRate)))
    file.write("tiempo antes: %f\n" %timebefore)
    file.write("count: %i\n" %frame_count)
    file.write("count before %i\n" %framebefore)
    file.write("velocidad: %f\n" %tmp_trk.velocity[-1])
    file.write("-------------------------------------\n")
    file.close()
    #if distancePix>0.9:
    # tmp_trk.centroids.append(centroid)
    # tmp_trk.frameNum.append(frame_count)

    #if len(tmp_trk.velocity)>10:
    media=np.median(np.asarray(tmp_trk.velocity))
    delt_vel=tmp_trk.velocity[-1]-media
	#tmp_trk.aceleration.append(delt_vel)
        #delt_vel=float(tmp_trk.velocity[-1]-tmp_trk.velocity[-10])
    tmp_trk.aceleration.append(delt_vel)
        #vel1=tmp_trk.velocity[-3]
        #vel2=tmp_trk.velocity[-1]
    #else:
	#media=np.median(np.asarray(tmp_trk.velocity))
        #delt_vel=tmp_trk.velocity[-1]-media
	#tmp_trk.aceleration.append(delt_vel)
	#delt_vel=float(tmp_trk.velocity[-1]-tmp_trk.velocity[-2])
        #tmp_trk.aceleration.append(delt_vel)
        #vel1=tmp_trk.velocity[-2]
        #vel2=tmp_trk.velocity[-1]


    if tmp_trk.id=="AV":
	matrizResult=[]
	matrizResult.append(np.array(tmp_trk.velocity))
	#matrizResult.append(np.asarray(tmp_trk.centroidTrans2D)[:,0])
	#matrizResult.append(np.array(tmp_trk.centroidTrans2D)[:,1])
	matrizResult=np.array(matrizResult)
        file=open("salidaCarro.txt","a")
        file.write("--------------------------------------------")
        file.write("Tiempo de ejecu: %f\n" %(float(frame_count)/frameRate))
	file.write("Instance velocity:%10.10f "%instanVelocity)
	file.write("aceleración: \n")
	np.savetxt(file, np.vstack(tmp_trk.aceleration), fmt='%1.10f')
	file.write("velocidad\n")
        #np.savez(file,np.array(tmp_trk.velocity),np.array(tmp_trk.centroidTrans2D)[:,0])
	np.savetxt(file, np.vstack(tmp_trk.velocity), fmt='%1.10f')
        file.write("matched \n")
	np.savetxt(file,np.vstack(tmp_trk.matched),fmt='%1.10f')
	file.write("centroid \n")
        np.savetxt(file, np.vstack(tmp_trk.centroidTrans2D), fmt='%1.10f')
	file.write("angulo \n")
        np.savetxt(file,np.vstack(tmp_trk.angleDirection), fmt='%1.10f')
	#file.write("distancia punto de cruce\n ")
	#np.savetxt(file,np.vstack(tmp_trk.crossPoint),fmt='%1.10f')
	file.write("dirección \n")
        file.write("direc %s\n "%tmp_trk.direction)

	file.write("-----------------------------------------------")
        file.close()

    file=open("salidaCarroTodos.txt","a")
    file.write("--------------------------------------------")
    file.write("Tiempo de ejecu: %f\n" %(float(frame_count)/frameRate))
    file.write("Instance velocity:%10.10f "%instanVelocity)
    file.write("aceleración: \n")
    np.savetxt(file, np.vstack(tmp_trk.aceleration), fmt='%1.10f')
    file.write("velocidad\n")
    #np.savez(file,np.array(tmp_trk.velocity),np.array(tmp_trk.centroidTrans2D)[:,0])
    np.savetxt(file, np.vstack(tmp_trk.velocity), fmt='%1.10f')
    file.write("matched \n")
    np.savetxt(file,np.vstack(tmp_trk.matched),fmt='%1.10f')
    file.write("centroid \n")
    np.savetxt(file, np.vstack(tmp_trk.centroidTrans2D), fmt='%1.10f')
    file.write("angulo \n")
    np.savetxt(file,np.vstack(tmp_trk.angleDirection), fmt='%1.10f')
    #file.write("distancia punto de cruce\n ")
    #np.savetxt(file,np.vstack(tmp_trk.crossPoint),fmt='%1.10f')
    file.write("dirección \n")
    file.write("direc %s\n "%tmp_trk.direction)

    file.write("-----------------------------------------------")
    file.close()



    return tmp_trk

def calculate_Distance2cross(imgray,xx,trk):
    if len(streetList)!=0:

	 maskcar=np.zeros_like(cv2.cvtColor(imgray, cv2.COLOR_BGR2GRAY))
         box_car=np.array([[xx[1],xx[0]],[xx[3],xx[0]],[xx[3],xx[2]],[xx[1],xx[2]]])
         cv2.fillPoly(maskcar,[box_car],[20])
         assign_streets_to_trackers(trk,maskcar,box_car)
         if trk.histoStreet[-1]!="NaN":
             if len(trk.histoStreet)>1:
                if trk.histoStreet[-2] != trk.histoStreet[-1]:
                    trk.crossPoint=[]

             vectdis=[]
             #for idx,cent in enumerate(trk.street.interseCentroids):
	     for idx,cent in enumerate(trk.street.interseCentroidsTransf):
                  # dist=np.sqrt(sum((cent-trk.centroids[-1])**2))
		  #dist=np.sqrt(sum((cent-trk.centroidTrans2D[-1])**2))
                  if trk.direction=="V":
			posCentInt=cent[1]
			posCentCar=trk.centroidTrans2D[-1][1]
         	  elif trk.direction=="H":
                	posCentInt=cent[0]
                        posCentCar=trk.centroidTrans2D[-1][0]

                  dist=abs(posCentInt-posCentCar)

		  vectdis.append(dist)
                  if dist < 40.0:
                    trk.colorProb=[255,0,0]

             trk.crossPoint.append(vectdis)
    else:
        trk.crossPoint.append(200)
    return trk

def probability_of_collision(trk):

    flagV=False
    flagD=False
    flagC=False
    flagP=False
    dtvelinf=-30.0
    dtvelsup=0.0
    dtposxinf=0.0
    dtposxsup=7.0
    dtposyinf=0.0
    dtposysup=7.0
    dtdirinf=0.0
    dtdirsup=18.0
    dtcrossinf=25.0
    dtcrosssup=50.0

    VF,PF,DF,CF=0.0,0.0,0.0,0.0

    #location=pathVideo[:-1]
    #if location=="camaraTunel":
    #	local=1
    #else:
    #	local=randint(2,4)


    if len(trk.histoStreet)>0 and trk.histoStreet[0]!="NaN":
        crossDif=np.array(trk.crossPoint[-1])
        print crossDif
	if len(crossDif)!=0:
	   delt_cross=crossDif.min()
	else:
	   delt_cross=dtcrosssup+1
    else:
        delt_cross=dtcrosssup+1

    #-----------------------------------------
    
    if streetInOneDir:
	    if direction=="V":
		if trk.street.wayDirection=="U":
		   delt_dir=abs(180.0-float(trk.angleDirection[-1]))

                   if dtdirinf<=delt_dir<=dtdirsup:
                      DF=(1.0/dtdirsup)*delt_dir
                   elif delt_dir>dtdirsup:
                      DF=1.0
                   else:
                      DF=0.0

                   #if DF>0.2 and len(trk.angleDirection)>6:
                   #  trk.numChangDir=trk.numChangDir+1

                   #if trk.numChangDir>4:
                   #   flagD=True

		if trk.street.wayDirection=="D":
		   delt_dir=abs(0.0-float(trk.angleDirection[-1]))
                   if dtdirinf<=delt_dir<=dtdirsup:
                      DF=(1.0/dtdirsup)*delt_dir
                   elif delt_dir>dtdirsup:
                      DF=1.0
                   else:
                      DF=0.0

                   #if DF>0.2 and len(trk.angleDirection)>6:
                   #   trk.numChangDir=trk.numChangDir+1

                   #if trk.numChangDir>4:
                   #   flagD=True

	    if direction=="H":
		if trk.street.wayDirection=="L":
		   delt_dir=abs(90.0-float(trk.angleDirection[-1]))
                   if dtdirinf<=delt_dir<=dtdirsup:
                      DF=(1.0/dtdirsup)*delt_dir
                   elif delt_dir>dtdirsup:
                      DF=1.0
                   else:
                      DF=0.0

                   #if DF>0.2 and len(trk.angleDirection)>6:
                   #   trk.numChangDir=trk.numChangDir+1

                   #if trk.numChangDir>4:
                   #   flagD=True

		if trk.street.wayDirection=="R":
		   delt_dir=abs(270.0-float(trk.angleDirection[-1]))
                   if dtdirinf<=delt_dir<=dtdirsup:
                      DF=(1.0/dtdirsup)*delt_dir
                   elif delt_dir>dtdirsup:
                      DF=1.0
                   else:
                      DF=0.0

                   #if DF>0.2 and len(trk.angleDirection)>6:
                   #   trk.numChangDir=trk.numChangDir+1

                   #if trk.numChangDir>4:
                   #   flagD=True

    else:
	    delt_dir=0.0
	    if trk.direction=="V" and trk.carWay=="up":
		delt_dir=abs(180.0-float(trk.angleDirection[-1]))

	    	if dtdirinf<=delt_dir<=dtdirsup:
	        	DF=(1.0/dtdirsup)*delt_dir
	    	elif delt_dir>dtdirsup:
			DF=1.0
	    	else:
	        	DF=0.0

		#if DF>0.2 and len(trk.angleDirection)>6:
		#   trk.numChangDir=trk.numChangDir+1

		#if trk.numChangDir>4:
		#   flagD=True

	    if trk.direction=="V" and trk.carWay=="down":

	        delt_dir=abs(0.0-float(trk.angleDirection[-1]))
	        if dtdirinf<=delt_dir<=dtdirsup:
	                DF=(1.0/dtdirsup)*delt_dir
	        elif delt_dir>dtdirsup:
	                DF=1.0
	        else:
	                DF=0.0

	        #if DF>0.2 and len(trk.angleDirection)>6:
	        #   trk.numChangDir=trk.numChangDir+1

	        #if trk.numChangDir>4:
	        #   flagD=True


	    if trk.direction=="H" and trk.carWay=="left":

		delt_dir=abs(90.0-float(trk.angleDirection[-1]))
		if dtdirinf<=delt_dir<=dtdirsup:
	                DF=(1.0/dtdirsup)*delt_dir
	        elif delt_dir>dtdirsup:
	                DF=1.0
	        else:
	                DF=0.0

	        #if DF>0.2 and len(trk.angleDirection)>6:
	        #   trk.numChangDir=trk.numChangDir+1

	        #if trk.numChangDir>4:
	        #   flagD=True

	    if trk.direction=="H" and trk.carWay=="right":

	        delt_dir=abs(270.0-float(trk.angleDirection[-1]))
	        if dtdirinf<=delt_dir<=dtdirsup:
	                DF=(1.0/dtdirsup)*delt_dir
	        elif delt_dir>dtdirsup:
	                DF=1.0
	        else:
	                DF=0.0

	        #if DF>0.2 and len(trk.angleDirection)>6:
	        #   trk.numChangDir=trk.numChangDir+1

	        #if trk.numChangDir>4:
	        #   flagD=True
    if DF>0.4 and len(trk.angleDirection)>6:
        trk.numChangDir=trk.numChangDir+1
    else:
        if trk.numChangDir!=0:
                trk.numChangDir=trk.numChangDir-1

    if trk.numChangDir>4:
       flagD=True
    else:
       flagD=False
    #-----------------------------------------
    
    if trk.street.direction=="V":
	if (trk.possibleAccident==True and delt_dir<8.0):
		mediax=np.median(np.asarray(trk.posx[-10:-1]))
	else:
		mediax=np.median(np.asarray(trk.posx))
	delt_posx=abs(mediax-trk.posx[-1])
	delt_pos=delt_posx
	mediaPos=mediax

    	if dtposxinf<=abs(delt_posx)<=dtposxsup:
        	#PF=(-1.0/dtposxsup)*abs(delt_posx)+1
        	PF=(1.0/dtposxsup)*abs(delt_posx)

	elif  abs(delt_posx)>dtposxsup:
        	PF=1.0
        else:
        	PF=0.0
        	flagP=False

    elif trk.street.direction=="H":
	if (trk.possibleAccident==True and delt_dir<8.0):
		mediay=np.median(np.asarray(trk.posy[-10:-1]))
	else:
		mediay=np.median(np.asarray(trk.posy))
        delt_posy=abs(mediay-trk.posy[-1])
	delt_pos=delt_posy
	mediaPos=mediay

    	if dtposyinf<=abs(delt_posy)<=dtposysup:
        	#PF=(-1.0/dtposysup)*abs(delt_posy)+1
        	PF=(1.0/dtposysup)*abs(delt_posy)
    	elif abs(delt_posy)>dtposysup:
		PF=1.0
    	else:
        	PF=0.0
        	flagP=False
    else:
	mediaPos=0
	PF=0.0
	flagP=False

    if delt_pos==0.0  and DF>0.96:
	PF=1

    if PF>0.4 and len(trk.centroidTrans2D)>5:
       trk.numChangPos=trk.numChangPos+1
    else:
        if trk.numChangPos!=0:
                trk.numChangPos=trk.numChangPos-1

    if trk.numChangPos>4:
       flagP=True
    else:
       flegP=False
    #-----------------------------------------
    mediaVel=np.median(np.asarray(trk.velocity))
    delt_vel=trk.velocity[-1]-mediaVel
    if dtvelinf<=delt_vel<=dtvelsup:
        VF=(1.0/dtvelinf)*delt_vel
    elif delt_vel<dtvelinf:
	VF=1.0
    else:
        VF=0.0
        flagV=False

    if trk.velocity[-1]<=0.1  and DF>0.96:
	VF=1

    if VF>0.4 and len(trk.velocity)>5:
       trk.numChangVel=trk.numChangVel+1
    else:
	if trk.numChangVel!=0:
		trk.numChangVel=trk.numChangVel-1

    if trk.numChangVel>4:
       flagV=True
    else:
       flagV=False
    #-----------------------------------------

    if dtcrossinf<=abs(delt_cross)<=dtcrosssup:
        #CF=(-1.0/dtcrosssup)*abs(delt_cross)+1
	CF=((-1.0/(dtcrosssup-dtcrossinf))*abs(delt_cross))+(dtcrosssup/(dtcrosssup-dtcrossinf))

    elif abs(delt_cross)<dtcrossinf:
	CF=1.0
    else:
        CF=0.0
        flagC=False
    if CF>0.4 and len(trk.crossPoint)>5:
	trk.numChangCross=trk.numChangCross +1
    else:
        if trk.numChangCross!=0:
                trk.numChangCross=trk.numChangCross-1

    if trk.numChangCross>3:
	flagC=True
    else:
	flagC=False
    #---------------------------------------------------------------------------------
    if streetInOneDir:
    	if trk.street.direction=="V" and trk.direction == "H":
		trk.turn=trk.turn+1
    	elif trk.street.direction=="H" and trk.direction == "V":
		trk.turn=trk.turn+1
    	else:
	     if trk.turn!=0:
	   	trk.turn=trk.turn-1
    else:
	trk.turn=0
    #---------------------------------------------------------------------------------
    if trk.turn<10:
    	if not streetInOneDir:
    		trk.probability.append((VF+PF+DF+CF)/4.0)
    	else:
		trk.probability.append((VF+PF+DF)/3.0)
    else:
	#trk.probability.append(1)
	#trk.numPossibleAccid=60
	#trk.numTurn=trk.numTurn+1
	print "hola"
    #-------------------------------------------------------------------------------
    if trk.probability[-1]>0.8:#0.6
	trk.possibleAccident=True
	trk.numPossibleAccid=trk.numPossibleAccid + 1
	#trk.numNotAccident=trk.numNotAccident + 1
	#if trk.velocity[-1]<12.0 and trk.probability[-1]>0.8:
	#	trk.numPossibleAccid=trk.numPossibleAccid + 1
    else:
	if trk.numPossibleAccid!=0:
		trk.numPossibleAccid=trk.numPossibleAccid - 1
			
    accident=15
    if trk.numPossibleAccid>accident and trk.flagAccident==False:
       trk.accident=True
       trk.flagAccident=True
    else:
       trk.accident=False
    #----------------------Data base--------------------------------------------------
    
    minDistAccid=0
    if len(trackerAccidentedList)==0:
    	if trk.accident==True and trk.numTurn<10 and trk.sendAccident==False and trk.street.id!=40 and trk.street.id!=30:
	    #db = MySQLdb.connect(host="localhost",user="root",passwd="root",db="accident_patterns")
	    #cur = db.cursor()
	    trackerAccidentedList.append(trk.centroidTrans2D[-1])
	    trk.possibleAccident=False
	    cur.execute("INSERT INTO accident_events(id,time,probability,accident,id_pattern,id_vehicles,id_location) VALUES (NULL, CURRENT_TIMESTAMP,%s, %s,%s, %s,%s)",(str(100),str(1),str(1),str(1),str(local)))
	    db.commit()
	    trk.sendAccident=True

   	elif trk.accident==False and trk.possibleAccident==True and trk.numPossibleAccid<=accident and trk.numPossibleAccid>3  and trk.sendPossiAccident==False and trk.street.id!=40 and trk.street.id!=30 and trk.sendAccident==False:
	    #db = MySQLdb.connect(host="localhost",user="root",passwd="root",db="accident_patterns")
            #cur = db.cursor()
	    print "mala maniobra"
	    cur.execute("INSERT INTO accident_events(id,time,probability,accident,id_pattern,id_vehicles,id_location) VALUES (NULL, CURRENT_TIMESTAMP,%s, %s,%s, %s,%s)",(str(trk.probability[-1]*100),str(0),str(1),str(1),str(local)))
            db.commit()
	    trk.sendPossiAccident=True
    else:
	vectdis=[]	
	for idx,cent in enumerate(trackerAccidentedList):
             dist=np.sqrt(sum((cent-trk.centroidTrans2D[-1])**2))
             vectdis.append(dist)

	vectdismin=np.array(vectdis)
        if len(vectdismin)!=0:
           minDistAccid=vectdismin.min()
	
	
	if trk.accident==True and trk.numTurn<2 and trk.sendAccident==False and trk.street.id!=40 and trk.street.id!=30 and minDistAccid>60:
            #db = MySQLdb.connect(host="localhost",user="root",passwd="root",db="accident_patterns")
            #cur = db.cursor()
            trackerAccidentedList.append(trk.centroidTrans2D[-1])
            trk.possibleAccident=False
            cur.execute("INSERT INTO accident_events(id,time,probability,accident,id_pattern,id_vehicles,id_location) VALUES (NULL, CURRENT_TIMESTAMP,%s, %s,%s, %s,%s)",(str(100),str(1),str(1),str(1),str(local)))
            db.commit()
            trk.sendAccident=True

        elif trk.accident==False and trk.possibleAccident==True and trk.numPossibleAccid<=accident and trk.numPossibleAccid>3  and trk.sendPossiAccident==False and trk.street.id!=40 and trk.street.id!=30 and trk.sendAccident==False and minDistAccid>60:
            #db = MySQLdb.connect(host="localhost",user="root",passwd="root",db="accident_patterns")
            #cur = db.cursor()
            print "mala maniobra"
            cur.execute("INSERT INTO accident_events(id,time,probability,accident,id_pattern,id_vehicles,id_location) VALUES (NULL, CURRENT_TIMESTAMP,%s, %s,%s, %s,%s)",(str(trk.probability[-1]*100),str(0),str(1),str(1),str(local)))
            db.commit()
            trk.sendPossiAccident=True 
    
    #----------------------------------------------------------------------------------

    file=open("logs.txt","a")
    file.write("-------------------------------------\n")
    file.write("car: %s\n" %trk.id)
    file.write("vector velo: \n")
    np.savetxt(file, np.vstack(trk.velocity), fmt='%1.10f')
    file.write("Tiempo de ejecu: %f\n" %(float(frame_count)/frameRate))
    file.write("banderas V: "+str(flagV)+"\n")
    file.write("banderas P: "+str(flagP)+"\n")
    file.write("banderas C: "+str(flagC)+"\n")
    file.write("banderas D: "+str(flagD)+"\n")
    file.write("street: %i\n" %trk.street.id)
    file.write("angle 2: %f\n" %trk.angleDirection[-1])
    file.write("angle 1: %f\n" %trk.angleDirection[-2])
    file.write("direction: %s\n" %trk.direction)
    file.write("delta dir: %f\n" %delt_dir)
    file.write("probabity DF: %f\n" %DF)
    file.write("delta vel: %f\n" %delt_vel)
    file.write("velocidad1: %f\n" %trk.velocity[-1])
    file.write("mediaVel:  %f\n" %mediaVel)
    file.write("probabity VF: %f\n" %VF)
    file.write("delta pos: %f\n" %delt_pos)
    file.write("probabity PF: %f\n" %PF)
    file.write("delta cross: %f\n" %delt_cross)
    file.write("probabity CF: %f\n" %CF)
    file.write("probabity: %f\n" %trk.probability[-1])
    file.close()

    if trk.id=="AV":
        file=open("salidaProba.txt","a")
        file.write("--------------------------------------------\n")
        file.write("id : "+str(trk.id)+"\n")
	file.write("frames: %f \n "%frame_count)
	file.write("accident: "+str(trk.accident)+"\n")
	file.write("posible acci: "+str(trk.possibleAccident)+"\n")
	file.write("num pos acci: %f "%trk.numPossibleAccid)
	file.write("sin cruces: "+str(streetInOneDir)+"\n")
	file.write("Tiempo de ejecu: %f\n" %(float(frame_count)/frameRate))
	file.write("datos X \n" )
	file.write("media= %f \n"%mediaPos)
	file.write("banderas V: "+str(flagV)+"\n")
    	file.write("banderas P: "+str(flagP)+"\n")
    	file.write("banderas C: "+str(flagC)+"\n")
    	file.write("banderas D: "+str(flagD)+"\n")
    	file.write("delta vel: %f\n" %delt_vel)
    	file.write("velocidad1: %f\n" %trk.velocity[-1])
    	file.write("mediaVel: %f\n" %mediaVel)
    	file.write("probabity VF: %f\n" %VF)
    	file.write("delta pos: %f\n" %delt_pos)
    	file.write("probabity PF: %f\n" %PF)
    	file.write("delta cross: %f\n" %delt_cross)
    	file.write("probabity CF: %f\n" %CF)
	file.write("delta dir: %f\n" %delt_dir)
        file.write("probabity DF: %f\n" %DF)
    	file.write("probabity: %f\n" %trk.probability[-1])
        file.write("-----------------------------------------------")
        file.close()
    
    file=open("salidaProbaTodos.txt","a")
    file.write("--------------------------------------------\n")
    file.write("id : "+str(trk.id)+"\n")
    file.write("id street : "+str(trk.street.id)+"\n")
    #file.write("distacia al choque: %f "%minDistAccid)
    file.write("accident send : "+str(trk.sendAccident)+"\n")
    file.write("Posi accide send : "+str(trk.sendPossiAccident)+"\n") 
    file.write("frames: %f \n "%frame_count)
    file.write("accident: "+str(trk.accident)+"\n")
    file.write("posible acci: "+str(trk.possibleAccident)+"\n")
    file.write("num pos acci: %f "%trk.numPossibleAccid)
    file.write("sin cruces: "+str(streetInOneDir)+"\n")
    file.write("Tiempo de ejecu: %f\n" %(float(frame_count)/frameRate))
    file.write("datos X \n" )
    file.write("media= %f \n"%mediaPos)
    file.write("banderas V: "+str(flagV)+"\n")
    file.write("banderas P: "+str(flagP)+"\n")
    file.write("banderas C: "+str(flagC)+"\n")
    file.write("banderas D: "+str(flagD)+"\n")
    file.write("delta vel: %f\n" %delt_vel)
    file.write("velocidad1: %f\n" %trk.velocity[-1])
    file.write("mediaVel: %f\n" %mediaVel)
    file.write("probabity VF: %f\n" %VF)
    file.write("delta pos: %f\n" %delt_pos)
    file.write("probabity PF: %f\n" %PF)
    file.write("delta cross: %f\n" %delt_cross)
    file.write("probabity CF: %f\n" %CF)
    file.write("delta dir: %f\n" %delt_dir)
    file.write("probabity DF: %f\n" %DF)
    file.write("probabity: %f\n" %trk.probability[-1])
    file.write("-----------------------------------------------")
    file.close()


    return trk



def pipeline(img):
#def pipeline(img,file):
    '''
    Pipeline function for detection and tracking
    '''
    global frame_count
    global tracker_list
    global tracker_list_10f
    global max_age
    global min_hits
    global track_id_list
    global debug
    global streetList
    #global pathVideo
    global matrixTransf
    global streetInOneDir
    global direction

    frame_count+=1

    if (frame_count==1):
        transform=Perspective.Transform_Perspective(img,pathVideo)
        matrixTransf=transform.getTransformationMatrix()
	stee=SelectStreets.Sel_Streets(img,pathVideo,matrixTransf)
        stee.streets_rois()
        streetList=stee.streetList
	V=0
	H=0
	VH=0
	for idx,streetDir in enumerate(streetList):

		if streetDir.direction == "V":
		   V=V+1
		elif streetDir.direction=="H":
	 	   H=H+1
		else:
		   VH=VH+1

	if len(streetList)==V :
	   streetInOneDir=True
	   direction="V"
        elif len(streetList)==H:
	   streetInOneDir=True
  	   direction="H"
	else:
	   streetInOneDir=False

	file=open("salidaProba.txt","a")
        file.write("--------------------------------------------\n")
        file.write("V: %f\n "%V)
	file.write("H: %f\n "%H)
	file.write("VH: %f\n "%VH)
	file.write("lenLis: %f\n "%len(streetList))
        file.write("sin cruces: "+str(streetInOneDir)+"\n")
	file.close()

    if withDarknet:
        if withImages:
            z_box = det.get_localizationYolo(img,file) # measurement
        else:
            image=cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
            cv2.imwrite("image.jpg",image)
            z_box = det.get_localizationYolo(img,'image.jpg') # measurement
    else:
        z_box = det.get_localization(img) # measurement


    img_dim = (img.shape[1], img.shape[0])
    #z_box = det.get_localization(img) # measurement
    if debug:
       print('Frame:', frame_count)

    x_box =[]
    if debug:
        for i in range(len(z_box)):
           img1= helpers.draw_box_label(img, z_box[i], box_color=(255, 0, 0))
           plt.imshow(img1)
        plt.show()

    if len(tracker_list) > 0:
        for trk in tracker_list:
            x_box.append(trk.box)



    matched, unmatched_dets, unmatched_trks \
    = assign_detections_to_trackers(x_box, z_box, iou_thrd = 0.3)#0.3
    if debug:
         print('Detection:', z_box)
         print('x_box: ', x_box)
         print('matched:', matched)
         print('unmatched_det:', unmatched_dets)
         print('unmatched_trks:', unmatched_trks)


    # Deal with matched detections
    if matched.size >0:
        for trk_idx, det_idx in matched:
            z = z_box[det_idx]
            z = np.expand_dims(z, axis=0).T
            tmp_trk= tracker_list[trk_idx]
            tmp_trk.kalman_filter(z)
            xx = tmp_trk.x_state.T[0].tolist()
            if velConstant:
                xx =[xx[0], xx[2], xx[4], xx[6]]
            else:
                xx =[xx[0], xx[3], xx[6], xx[9]]

            #---------------------------------------------------------
            # cx=xx[1]+((xx[3]-xx[1])/2)
            # cy=xx[0]+((xx[2]-xx[0])/2)
            # centroid=np.array([cx,cy])
            #
            # if len(tmp_trk.centroids)>10:
            #     centroid_old=tmp_trk.centroids[len(tmp_trk.centroids)-9]
            # else:
            #     centroid_old=tmp_trk.centroids[len(tmp_trk.centroids)-1]
            #
            # distancePix=np.sqrt(sum((centroid-centroid_old)**2))
            # distanceUni=distancePix*scale
            # timeTravel=(np.float(frame_count)/np.float(frameRate))-(tmp_trk.frameNum[len(tmp_trk.frameNum)-1]/frameRate)
            # tmp_trk.velocity.append(distanceUni/timeTravel)
            #
            # if distancePix>0.9:
            #     tmp_trk.centroids.append(centroid)
            # tmp_trk.frameNum.append(frame_count)
            #----------------------------------------------------------

            tracker_list_10f[trk_idx]=copy.copy(tmp_trk)
            x_box[trk_idx] = xx
            tmp_trk.box =xx
	    tmp_trk.matched
	    tmp_trk.matched.append(1)
            tmp_trk.hits += 1


    # Deal with unmatched detections
    if len(unmatched_dets)>0:
        for idx in unmatched_dets:
            z = z_box[idx]
            z = np.expand_dims(z, axis=0).T
            tmp_trk = tracker.Tracker() # Create a new tracker
            if velConstant:
                x = np.array([[z[0], 0, z[1], 0, z[2], 0, z[3], 0]]).T
            else:
                x = np.array([[z[0], 0, 0, z[1], 0, 0, z[2], 0, 0, z[3], 0, 0]]).T
            tmp_trk.x_state = x
            tmp_trk.predict_only()
            xx = tmp_trk.x_state
            xx = xx.T[0].tolist()
            if velConstant:
                xx =[xx[0], xx[2], xx[4], xx[6]]
            else:
                xx =[xx[0], xx[3], xx[6], xx[9]]

            #-------fist time-------------------
            cx=xx[1]+((xx[3]-xx[1])/2)
            cy=xx[0]+((xx[2]-xx[0])/2)
            centroid=np.array([cx,cy])

            centroidTrans=matrixTransf.dot(np.expand_dims(np.array([cx,cy,1]), axis=0).T)
            centroidTrans2D = centroidTrans / np.tile(centroidTrans[-1, :], (3, 1)) # Divide todo por el valor último del vector.
            centroidTrans2D=centroidTrans2D[:2,0]
            tmp_trk.centroidTrans2D.append(centroidTrans2D)
            tmp_trk.posx.append(centroidTrans2D[0])
	    tmp_trk.posy.append(centroidTrans2D[1])

            tmp_trk.centroids.append(centroid)
            tmp_trk.frameNum.append(frame_count)
            #------------------------------------------------

            tmp_trk.box = xx
            tmp_trk.id = track_id_list.popleft() # assign an ID for the tracker
            tmp_trk_10f=copy.copy(tmp_trk)
            tracker_list.append(tmp_trk)
            tracker_list_10f.append(tmp_trk_10f)
            x_box.append(xx)

    # Deal with unmatched tracks
    if len(unmatched_trks)>0:
        for trk_idx in unmatched_trks:
            tmp_trk = tracker_list[trk_idx]
            tmp_trk.no_losses += 1
	    tmp_trk.matched.append(0)
            tmp_trk.predict_only()
            xx = tmp_trk.x_state
            xx = xx.T[0].tolist()
            if velConstant:
                xx =[xx[0], xx[2], xx[4], xx[6]]
            else:
                xx =[xx[0], xx[3], xx[6], xx[9]]

            #---------------------------------------------
            # cx=xx[1]+((xx[3]-xx[1])/2)
            # cy=xx[0]+((xx[2]-xx[0])/2)
            # centroid=np.array([cx,cy])
            #
            # if len(tmp_trk.centroids)>10:
            #     centroid_old=tmp_trk.centroids[len(tmp_trk.centroids)-9]
            # else:
            #     centroid_old=tmp_trk.centroids[len(tmp_trk.centroids)-1]
            #
            # distancePix=np.sqrt(sum((centroid-centroid_old)**2))
            # distanceUni=distancePix*scale
            # timeTravel=(np.float(frame_count)/np.float(frameRate))-(tmp_trk.frameNum[len(tmp_trk.frameNum)-1]/frameRate)
            # tmp_trk.velocity.append(distanceUni/timeTravel)
            #
            # # filter neared centroids
            # if distancePix>0.9:
            #     tmp_trk.centroids.append(centroid)
            # tmp_trk.frameNum.append(frame_count)
            #------------------------------------------------------------------------

            tmp_trk.box =xx
            #tracker_list_10f[trk_idx]=copy.copy(tmp_trk)
            x_box[trk_idx] = xx


    # The list of tracks to be annotated
    good_tracker_list =[]
    good_tracker_Probabi=[]
    good_tracker_list_10f=[]
    imgray=img.copy()
    impersp=img.copy()
    height, width = image.shape[:2]
    maxHeight=100
    warped = cv2.warpPerspective(impersp,matrixTransf, (width,height+maxHeight))
    for trk_idx,trk in enumerate(tracker_list):
    #for trk in tracker_list:
        if ((trk.hits >= min_hits) and (trk.no_losses <=max_age)):
             good_tracker_list.append(trk)
             good_tracker_list_10f.append(tracker_list_10f[trk_idx])

             #--------------collision features-----------------
             xx=trk.box

             warped, trk,success,distancePix=calculate_centroid(xx,trk,warped)
             # if len(trk.frameNum)>0 and success:
             #   trk=calculate_velocity(trk,distancePix)

             if success:
                 img=car_direction(trk,img)
		 if len(trk.frameNum)>0:
		    trk=calculate_velocity(trk,distancePix)

                 if len(trk.angleDirection)>2:
                    trk=calculate_Distance2cross(imgray,xx,trk)
                    trk=probability_of_collision(trk)
             #--------------------------------------------------
             x_cv2 =trk.box
	     good_tracker_Probabi.append(trk)

             if debug:
                 print('updated box:', x_cv2)
             img= helpers.draw_box_label(img, x_cv2,trk.box_color,trk.velocity,trk.id,trk.street,trk.colorProb,trk) # Draw the bounding boxes on the
             #img= helpers.draw_line_traker(img,trk.centroids,trk.box_color)
             img= helpers.draw_poliLine_traker(img,trk.centroids,trk.box_color)
                                             # images


    # imgray=img.copy()
    # for trk_idx,trk in enumerate(good_tracker_list):
    #     xx=trk.box
    #     maskcar=np.zeros_like(cv2.cvtColor(imgray, cv2.COLOR_BGR2GRAY))
    #     box_car=np.array([[xx[1],xx[0]],[xx[3],xx[0]],[xx[3],xx[2]],[xx[1],xx[2]]])
    #     cv2.fillPoly(maskcar,[box_car],[20])
    #     assign_streets_to_trackers(trk,maskcar,box_car)


    # lineas futuras con fitline
    # for trk_idx,trk_10f in enumerate(good_tracker_list_10f):
    #     x=[]
    #     y=[]
    #     samples=100
    #     if len(trk_10f.centroids)>samples:
    #         ini=len(trk_10f.centroids)-samples
    #         fin=len(trk_10f.centroids)-1
    #         futuro=60
    #     else:
    #         ini=0
    #         fin=len(trk_10f.centroids)-1
    #         futuro=10
    #
    #     points=np.array(trk_10f.centroids[ini:fin]).reshape((-1,1,2)).astype(np.int32)
    #     [vx,vy,x,y] = cv2.fitLine(points,cv2.DIST_L12,0,0.01,0.01)
    #
    #     slope = vy/vx
    #     intercept = y - (slope * x)
    #     X0 = trk_10f.centroids[ini][0]
    #     X1 = trk_10f.centroids[fin][0]
    #
    #     # para que la señal siga aumentando en el sentido
    #     # que lo viene haciendo
    #     if (X1-X0)>1:
    #         #step=1
    #         # no sobrepasar el tamaño de la imagen
    #         if (X1+futuro)<img.shape[1]:
    #             futuro=futuro
    #         else:
    #             futuro=img.shape[1]-X1
    #     elif (X1-X0)<0:
    #         #step=-1
    #         if (X1-futuro)>0:
    #             futuro=-futuro
    #         else:
    #             futuro=-X1 # el 2 es para evitar una posible posicion no existente
    #     else:
    #         continue
    #
    #     startX=X1
    #     endX=X1+futuro
    #     startY = (slope*startX)+intercept
    #     endY = (slope*endX)+intercept
    #     img=helpers.draw_line_future(img,[startX,startY],[endX,endY],(255,0,0))

    # # future lines with polyfit
    # for trk_idx,trk_10f in enumerate(good_tracker_list_10f):
    #     x=[]
    #     y=[]
    #     samples=30
    #     futuro=60
    #     if len(trk_10f.centroids)>=samples:
    #         ini=len(trk_10f.centroids)-samples
    #         fin=len(trk_10f.centroids)-1
    #         #futuro=15
    #     else:
    #         ini=0
    #         fin=len(trk_10f.centroids)-1
    #         #futuro=10
    #
    #     for idx in range(ini,fin):
    #         x.append(trk_10f.centroids[idx][0])
    #         y.append(trk_10f.centroids[idx][1])
    #     x=np.asarray(x)
    #     y=np.asarray(y)
    #     polino=np.poly1d(np.polyfit(x,y,1))
    #
    #     dist=np.sqrt(sum((np.array([x[0],y[0]])-np.array([x[len(x)-1],y[len(x)-1]]))**2))
    #
    #     if (x[len(x)-1]-x[0])>3:
    #         step=1
    #         if (x[len(x)-1]+futuro)<img.shape[1]:
    #             futuro=futuro
    #         else:
    #             futuro=img.shape[1]-x[len(x)-1]
    #     elif (x[len(x)-1]-x[0])<-3:
    #         step=2
    #         if (x[len(x)-1]-futuro)>0:
    #             futuro=-futuro
    #         else:
    #             futuro=-x[len(x)-1]
    #     else:
    #         step=3
    #         continue
    #     xin=x[len(x)-1]
    #     xfin=x[len(x)-1]+futuro
    #     trk_10f.xinf=xin
    #     trk_10f.xfinf=xfin
    #     img=helpers.draw_line_future(img,[xin,polino(xin)],[xfin,polino(xfin)],(255,0,0))
    #     if debug:
    #         print "--------------------------------"
    #         print "id",trk_10f.id
    #         print "centroides",trk_10f.centroids[ini:fin]
    #         print "xxxx",x
    #         print "yyyy",y
    #         print "futuro",futuro
    #         print "inicio",xin,xfin
    #         print "final",polino(xin),polino(xfin)
    #         print "step", step
    ##----------------------------------------------------------------------

    # future linea with kalman filter Predict K-step ahead
    # for trk_idx,trk_10f in enumerate(good_tracker_list_10f):
    #     tmp_trk_10f = trk_10f
    #     for i in range(0,1):
    #         tmp_trk_10f.predict_only()
    #         xx = tmp_trk_10f.x_state
    #         xx = xx.T[0].tolist()
    #         if velConstant:
    #             xx =[xx[0], xx[2], xx[4], xx[6]]
    #         else:
    #             xx =[xx[0], xx[3], xx[6], xx[9]]
    #         cx=xx[1]+((xx[3]-xx[1])/2)
    #         cy=xx[0]+((xx[2]-xx[0])/2)
    #         centroid=np.array([cx,cy])
    #         # if len(tmp_trk.centroids)>10:
    #         #     centroid_old=tmp_trk.centroids[len(tmp_trk.centroids)-9]
    #         # else:
    #         #     centroid_old=tmp_trk.centroids[len(tmp_trk.centroids)-1]
    #
    #         #centroid_old=tmp_trk_10f.centroids[len(tmp_trk_10f.centroids)-1]
    #         #distance=np.sqrt(sum((centroid-centroid_old)**2))*scale
    #         #timeTravel=(np.float(frame_count)/np.float(frameRate))-(tmp_trk_10f.frameNum[len(tmp_trk_10f.frameNum)-1]/frameRate)
    #         #tmp_trk.velocity=distance/timeTravel
    #         tmp_trk_10f.centroids_fut.append(centroid)
    #         #tmp_trk.frameNum.append(frame_count)
    #         tmp_trk_10f.box =xx
    #     img= helpers.draw_poliLine_traker(img,tmp_trk_10f.centroids_fut,(255,0,0))


    # Book keeping

    #-------------------------------------------------------------------

    #location=pathVideo[:-1]
    #if location=="camaraTunel":
    #    local=1
    #else:
    #    local=randint(2,4)


    for trk in good_tracker_Probabi:
	if trk.no_losses==max_age:           
	   minDistAccid=0
    	   if len(trackerAccidentedList)==0:
    		if trk.numPossibleAccid>18 and trk.street.id!=40 and trk.street.id!=30 and trk.sendAccident==False and trk.probability[-1]>0.90 and trk.probability[-2]>0.90:
	    		trackerAccidentedList.append(trk.centroidTrans2D[-1])
	    		trk.possibleAccident=False
	    		cur.execute("INSERT INTO accident_events(id,time,probability,accident,id_pattern,id_vehicles,id_location) VALUES (NULL, CURRENT_TIMESTAMP,%s, %s,%s, %s,%s)",(str(100),str(1),str(1),str(1),str(local)))
	    		db.commit()
	    		trk.sendAccident=True
		
		elif trk.numPossibleAccid>=4 and trk.probability[-1]>0.90 and trk.probability[-2]>0.90 and trk.street.id!=40 and trk.street.id!=30 and trk.sendAccident==False:
			trackerAccidentedList.append(trk.centroidTrans2D[-1])
	    		trk.possibleAccident=False
	    		cur.execute("INSERT INTO accident_events(id,time,probability,accident,id_pattern,id_vehicles,id_location) VALUES (NULL, CURRENT_TIMESTAMP,%s, %s,%s, %s,%s)",(str(100),str(1),str(1),str(1),str(local)))
	    		db.commit()
	    		trk.sendAccident=True
		
   		elif trk.accident==False and trk.possibleAccident==True and trk.numPossibleAccid<=10 and trk.numPossibleAccid>=3 and trk.probability[-1]<0.80  and trk.sendPossiAccident==False and trk.street.id!=40 and trk.street.id!=30 and trk.sendAccident==False and trk.probability[-1]>0.69:
	    		print "mala maniobra"
	    		cur.execute("INSERT INTO accident_events(id,time,probability,accident,id_pattern,id_vehicles,id_location) VALUES (NULL, CURRENT_TIMESTAMP,%s, %s,%s, %s,%s)",(str(trk.probability[-1]*100),str(0),str(1),str(1),str(local)))
            		db.commit()
	    		trk.sendPossiAccident=True
    	   else:
		vectdis=[]	
		for idx,cent in enumerate(trackerAccidentedList):
             		dist=np.sqrt(sum((cent-trk.centroidTrans2D[-1])**2))
             		vectdis.append(dist)

		vectdismin=np.array(vectdis)
        	if len(vectdismin)!=0:
           		minDistAccid=vectdismin.min()
	
	
		if trk.numPossibleAccid>10 and trk.street.id!=40 and trk.street.id!=30 and trk.sendAccident==False and minDistAccid>60:
            		trackerAccidentedList.append(trk.centroidTrans2D[-1])
            		trk.possibleAccident=False
            		cur.execute("INSERT INTO accident_events(id,time,probability,accident,id_pattern,id_vehicles,id_location) VALUES (NULL, CURRENT_TIMESTAMP,%s, %s,%s, %s,%s)",(str(100),str(1),str(1),str(1),str(local)))
            		db.commit()
            		trk.sendAccident=True
		
		elif trk.numPossibleAccid>4 and trk.probability[-1]>0.9 and trk.probability[-2]>0.9 and trk.street.id!=40 and trk.street.id!=30 and trk.sendAccident==False:
			trackerAccidentedList.append(trk.centroidTrans2D[-1])
	    		trk.possibleAccident=False
	    		cur.execute("INSERT INTO accident_events(id,time,probability,accident,id_pattern,id_vehicles,id_location) VALUES (NULL, CURRENT_TIMESTAMP,%s, %s,%s, %s,%s)",(str(100),str(1),str(1),str(1),str(local)))
	    		db.commit()
	    		trk.sendAccident=True

        	elif trk.accident==False and trk.possibleAccident==True and trk.numPossibleAccid<=5 and trk.sendPossiAccident==False and trk.street.id!=40 and trk.street.id!=30 and trk.sendAccident==False and minDistAccid>60:
            		print "mala maniobra"
            		cur.execute("INSERT INTO accident_events(id,time,probability,accident,id_pattern,id_vehicles,id_location) VALUES (NULL, CURRENT_TIMESTAMP,%s, %s,%s, %s,%s)",(str(trk.probability[-1]*100),str(0),str(1),str(1),str(local)))
            		db.commit()
            		trk.sendPossiAccident=True

    #-------------------------------------------------------------------

    deleted_tracks = filter(lambda x: x.no_losses >max_age, tracker_list)

    for trk in deleted_tracks:
            track_id_list.append(trk.id)

    tracker_list = [x for x in tracker_list if x.no_losses<=max_age]
    tracker_list_10f= [x for x in tracker_list_10f if x.no_losses<=max_age]

    if debug:
       print('Ending tracker_list: ',len(tracker_list))
       print('Ending good tracker_list: ',len(good_tracker_list))

    return img
    #return warped

if __name__ == "__main__":
    global pathVideo
    det = detector.CarDetector(withDarknet) 
    
    print(pathVideo)
    location=pathVideo[:-1]
    #if location=="camaraTunel1":
    #    local=1
    #else:
    #    #local=randint(2,4)
    #	local=2
    
    start=time.time()   
    nvid = os.path.splitext(sys.argv[1])[0]	

    if nvid=="camaraTunel1":
        local=1
    else:
        local=2

    voutput = '/var/www/mintic/accpatterns/static/o' + nvid + '.mp4'
    vinput = '/var/www/mintic/accpatterns/videoInputs/' + sys.argv[1]
    #fps = 30
    #cap = cv2.VideoCapture(random)
    cap = cv2.VideoCapture(vinput)
    #output = 'pruebas/salida11.mp4'
    pathVideo= nvid #"camara4"
    clip1 = VideoFileClip(vinput)#.subclip(4,49) # The first 8 seconds doesn't have any cars...
    clip1=clip1.set_fps(frameRate)
    clip = clip1.fl_image(pipeline)
    clip.write_videofile(voutput, audio=False,fps=frameRate)
    end  = time.time()
    print(round(end-start, 2), 'Seconds to finish')
