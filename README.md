VISION VIAL INTELIGENTE

INTRODUCCION

Este proyecto tiene como objetivo realizar un proceso de investigación, que
demuestre de forma teórica y práctica (a través de un prototipo funcional), las
potencialidades y los beneficios que puede generar la aplicación de tecnologías
emergentes para abordar el problema accidentalidad de las vías en una ciudad.

Especificamente, pretende aplicar vision computacional para, apartir de imagenes
de video recolectadas en las vias, identificar los patrones que generan accidentes
de transito.

DESCRIPCION DEL REPOSITORIO

Este proyecto contiene un contenedor Docker con una aplicacion aprovisionada que
implementa algoritmos de vision computacional para la deteccion de patrones que
generan accidentes. Dicho algoritmo esta escrito en python y usa OpenCV, Darknet,
y Yolo3. El cluster esta conformado por dos maquinas virtuales

COMO USAR ESTE REPOSITORIO

1. Descargar VirtualBox desde https://www.virtualbox.org/wiki/Downloads

2. Descargar e instalar desde http://www.vagrantup.com/downloads.html).

3. Ejecutar vagrant up para levantar el contenedor

4. En la maquina anfitriona abra http://localhost:8080 en un browser para
tener acceso al frontend de la aplicacion

