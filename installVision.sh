#!/bin/bash
# -*- ENCODING: UTF-8 -*-
# script instalaci�n de librerias proyecto MINTIC

# Update ubuntu
echo "Updating Ubuntu"
sudo apt-get update

#Install unzip
echo "Installing unzip"
sudo apt-get install unzip -y

# instalar python2.7
echo "Installing python 2.7"
export LC_ALL=C
sudo apt-get install python-pip python-dev -y
sudo pip install opencv-python
sudo pip install opencv-contrib-python==3.4.1.15

#instalar dependencias de c++ y python para opencv
echo "Installing C++ dependencies"
sudo apt-get install build-essential cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev -y

echo "Installing Other libs"
sudo apt-get install python3.5-dev python3-numpy -y 

sudo apt-get install libjpeg-dev libpng-dev libtiff5-dev libjasper-dev libdc1394-22-dev libeigen3-dev libtheora-dev libvorbis-dev libxvidcore-dev libx264-dev sphinx-common libtbb-dev yasm libfaac-dev libopencore-amrnb-dev libopencore-amrwb-dev libopenexr-dev libgstreamer-plugins-base1.0-dev libavutil-dev libavfilter-dev libavresample-dev -y

sudo apt-get install python-numpy libtbb2 libtbb-dev -y   

sudo apt-get install libatlas-base-dev gfortran pylint -y
sudo apt-get install python2.7-dev python3.5-dev -y


#descargar e instalar opencv
#cd /home
echo "Installing OpenCV"
wget https://github.com/opencv/opencv/archive/3.4.0.zip -O opencv-3.4.0.zip
unzip opencv-3.4.0.zip
cd opencv-3.4.0
mkdir build
cd build
sudo cmake ..
sudo make -j5
sudo make install
cd ../..
rm opencv-3.4.0.zip	

#instalar librerias para video
echo "Installing Video Libs"
sudo pip install moviepy
sudo add-apt-repository ppa:mc3man/trusty-media -y
sudo apt-get install ffmpeg -y
sudo apt-get install frei0r-plugins -y

#instalar otras librerias
sudo pip install scikit-learn
sudo pip install matplotlib==2.2.3
sudo pip install tensorflow
sudo pip install simplejson
sudo pip install pyyaml

#Instalar Apache y modulo wsgi
echo "Installing apache and wsgi module"
apt-get install apache2 apache2-utils libexpat1 ssl-cert python -y
apt-get install libapache2-mod-wsgi -y
a2enmod wsgi

echo "<VirtualHost *:80>
    ServerName www.visionvial.com
    Timeout 2400
    WSGIDaemonProcess accpatterns user=www-data group=www-data threads=5
    WSGIProcessGroup accpatterns
    WSGIScriptAlias / /var/www/mintic/accpatterns/application.wsgi
    Alias /static/ /var/www/mintic/accpatterns/static
    <Directory /var/www/mintic/accpatterns/static>
        Order allow,deny
        Allow from all
    </Directory>

</VirtualHost>" > /etc/apache2/sites-available/000-default.conf

chown -R www-data:www-data /var/www
chmod a+x /var/www/mintic/accpatterns/application.wsgi
/etc/init.d/apache2 reload
/etc/init.d/apache2 restart

sudo cp -r /vagrant/mintic /var/www/mintic
mkdir /var/www/mintic/accpatterns/static
chmod 755 /var/www/mintic/accpatterns/static
chown -R www-data:www-data /var/www

#Instalar MySQL & Flask
echo "Installing MySQL and Flask"

debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

apt-get install mysql-server -y
apt-get install python-mysqldb -y

pip install flask
pip install flask-mysqldb
pip install flask-mysql
pip install WTForms
pip install passlib
pip install flask_socketio
# install moviepy
pip install moviepy

# install ffmpeg
apt-get install ffmpeg -y
apt-get install frei0r-plugins -y
pip install requests
pip install ffmpy

service mysql start

#Creating Database
echo "Creating Database"
mysql --user=root --password=root <<MYSQL_SCRIPT
CREATE DATABASE accident_patterns;

USE accident_patterns;

CREATE TABLE accident_events (
  id int(11) NOT NULL AUTO_INCREMENT,
  time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  probability int(11) DEFAULT NULL,
  accident tinyint(1) DEFAULT NULL,
  id_pattern int(11) NOT NULL,
  id_vehicles int(11) NOT NULL,
  id_location int(11) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE locations (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(100) DEFAULT NULL,
  latitude double DEFAULT NULL,
  longitude double NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE patterns (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  description varchar(200) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE users (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(100) DEFAULT NULL,
  email varchar(100) DEFAULT NULL,
  username varchar(30) DEFAULT NULL,
  password varchar(100) DEFAULT NULL,
  register_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE vehicle_combinations (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  description varchar(200) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO patterns (id, name, description) VALUES
(1, 'exceso_velocidad', 'Accidente causado por exceso de velocidad de uno de los veh�culos'),
(2, 'direccion_abrupta', 'Accidente causado por cambio abrupto en la direccion de uno de los veh�culos');

INSERT INTO vehicle_combinations (id, name, description) VALUES
(1, 'carro_carro', 'Accidente involucra vehiculo de tipo carro contra vehiculo de tipo carro'),
(2, 'carro_moto', 'Accidente involucra vehiculo de tipo carro contra vehiculo de tipo moto'),
(3, 'moto_moto', 'Accidente involucra vehiculo de tipo moto contra vehiculo de tipo moto');

INSERT INTO locations(id,name,latitude,longitude) VALUES
(1,'Tunel Mundialista','3.453901','-76.532493'),
(2,'Universidad Autonoma de Occidente','3.454011','-76.522841');
MYSQL_SCRIPT

chown -R mysql:mysql /var/lib/mysql /var/run/mysqld
