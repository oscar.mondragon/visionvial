FROM littlejuan/ubuntu-ssh
MAINTAINER Oscar Mondragon
EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]

